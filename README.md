# xtesting-onap

leverage xtesting-onap-robot and xtesting-onap-vnf in CI/CD chains

https://hub.docker.com/r/morganrol/xtesting-onap-robot/
https://hub.docker.com/r/morganrol/xtesting-onap-vnf/

This project aims to automatically test ONAP. Its config source
is shared config files among all OPNFV installers:
- PDF - Pod Description File: describing the hardware level of the
  infrastructure hosting the VIM

## Input

  - configuration files:
    - mandatory:
        - vars/pdf.yml: POD Description File
        - vars/cluster.yml: information about ONAP cluster
        - inventory/jumphost: the ansible inventory for the jumphost
        - vars/kube-config: the kubernetes configuration file in order to have
          credentials to connect
        - clouds.yml: retrieve from the controler node used to create OpenStack
          resources when needed and verify the creation of resources through
          openstack commands. For xtesting, assuming that it is run from the
          controller node, it is transparent. If not copy the clouds.yml in the
          docker under /root/.config/openstack/clouds.yml and reference the
          cloud with the env variable OS_TEST_CLOUD
    - optional:
        - vars/vaulted_ssh_credentials.yml: Ciphered private/public pair of key
          that allows to connect to jumphost
  - Environment variables:
    - mandatory:
        - PRIVATE_TOKEN: to get the artifact
        - artifacts_src: the url to get the artifacts
        - OR artifacts_bin: b64_encoded zipped artifacts (tbd)
        - ANSIBLE_VAULT_PASSWORD: the vault password needed by ciphered ansible
          vars
        - TEST_CLOUD
          - role: name of the cloud as defined in the clouds.yaml_linting
          - value type: string
          - default: "openlab-vnfs-ci"
    - optional:
      - ANSIBLE_VERBOSE:
          - role: verbose option for ansible
          - values: "", "-vvv"
          - default: ""
      - POD:
          - role: name of the pod when we'll insert healtcheck results
          - value type;: string
          - default: empty
      - DEPLOYMENT:
          - role: name of the deployment for right tagging when we'll insert
            healtcheck results
          - value type: string
          - default: "oom"
      - INFRA_DEPLOYMENT:
          - role: name of the infra deployment for right tagging when we'll
            insert healtcheck results
          - value type: string
          - default: "rancher"
      - DEPLOYMENT_TYPE:
          - role: type of ONAP deployment done
          - values: "core", "small", "medium", "full"
          - default: "core" if nothing found in vars/cluster.yml
      - TEST_RESULT_DB_URL:
          - role: url of test db api
          - value type: string
          - default: "http://testresults.opnfv.org/onap/api/v1/results"
      - DEPLOY_SCENARIO
          - role: name of the deployment scenario
          - value type: string
          - default: "onap-ftw"
      - ONAP_NAMESPACE
          - role: the name of the namespace on kubernetes where ONAP is
            installed
          - value type: string
          - default: "onap"
      - ONAP_VERSION
          - role: the ONAP version deployed
          - value type: string
          - values: "beijing", "2.0.0-ONAP", "master"
          - default: "master"
      - tests_list
          - role: Define the vnf tests list
          - value type: string
          - values: "all", "basic_vm, freeradius_nbi, ims"
          - default: "all"    
      - DEBUG
          - role: enable debug logs and the creation of xtesting.debug.log
          - value type: boolean
          - values: True, False (case insensitive)
          - default: False

## Output

none
