#!/bin/bash

echo '______________ Results _____________'
echo '******** Kubernetes Results ********'
grep '>>>' "./results/k8s/onap-k8s.log" | tr ',' '\n' | sed 's/>>>/ */; s/\[\([^]]\)/[\'$'\n   - \\1/; s/^[^ ][^\*]*/   - &/'
echo '*********** Helm Results ***********'
grep '>>>' "./results/k8s/onap-helm.log" | tr ',' '\n' | sed 's/>>>/ */; s/\[\([^]]\)/[\'$'\n   - \\1/; s/^[^ ][^\*]*/   - &/'
echo '******** Healthcheck Results *******'
sed -n '/xtesting.core.robotframework - INFO - $/,/Output/p' "./results/healthcheck/full/xtesting.log"
echo '******** healthdist Results *******'
sed -n '/xtesting.core.robotframework - INFO - $/,/Output/p' "./results/healthcheck/healthdist/xtesting.log"
echo '____________________________________'
